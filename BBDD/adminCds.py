#Administración BBDD
import mysql.connector
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import * 
from BBDD.adminDiscograficas import * 

class AdminCds:

    def getAll(self,dataSource):
        #Generamos la Query
        query = ("SELECT * FROM cd")
        #Recogemos los datos que nos devuelve la consulta
        cds_bd = dataSource.executeQuerySelect(query)
        #adminDisco=AdminDiscografica()
        adminGenero = AdminGenero()
        #Creamos lista vacia para guardar objetos de los cds
        cds = []
        #Recorremos los datos recogidos anteriormente (matriz)
        for cd1 in cds_bd:
            #Creamos objetos de los cds 
            cd = Cd(cd1[0],cd1[1],cd1[2],cd1[3],cd1[4],cd1[5])
            #Los guardamos en la lista cds
            cd.setDiscografica(AdminDiscografica().getById(dataSource, cd.getIddiscagraficas()))
            cd.setGeneros(adminGenero.getGenerosByCD(dataSource, cd.getIdCD()))
            cds.append(cd)

        #Retornamos la lista de objetos de cds
        return cds

    def getById(self,dataSource, id):
        #Generamos la Query
        query = ("SELECT * FROM cd WHERE idCD = %i" %(id))

        #Recogemos los datos que nos devuelve la consulta
        cds_bd = dataSource.executeQuerySelect(query)
        adminDiscograficas = AdminDiscografica()
        adminGenero = AdminGenero()

        #Generamos un cd como none
        cd = None
        #Comprobamos que la consulta solo nos devulve una columna
        if len(cds_bd) == 1:
            #Recorremos los datos recogidos anteriormente (matriz)
            for cd1 in cds_bd:
                #Generamos el cd a partir del registro 
                cd = Cd(cd1[0],cd1[1],cd1[2],cd1[3],cd1[4],cd1[5])
                cd.setDiscografica(adminDiscograficas.getById(dataSource, cd.getIddiscagraficas()))
                cd.setGeneros(adminGenero.getGenerosByCD(dataSource, cd.getIdCD()))

        #Retornamos el cd encontrado
        return cd

    def getCDsByDiscografica(self,dataSource, iddiscograficas):
        #Generamos la Query
        query = ("SELECT * FROM cd WHERE iddiscograficas = %s" %(iddiscograficas))

        #Recogemos los datos que nos devuelve la consulta
        cds_bd = dataSource.executeQuerySelect(query)
        #adminDisco=AdminDiscografica()
        #adminGenero = AdminGenero()
        #Creamos lista vacia para guardar objetos de los cds
        cds = []
        #Recorremos los datos recogidos anteriormente (matriz)
        for cd1 in cds_bd:
            #Creamos objetos de los cds 
            cd = Cd(cd1[0],cd1[1],cd1[2],cd1[3],cd1[4],cd1[5])
            #Los guardamos en la lista cds
            cd.setDiscografica(AdminDiscografica().getById(dataSource, cd.getIddiscagraficas()))
            cd.setGeneros(AdminGenero().getGenerosByCD(dataSource, cd.getIdCD()))
            cds.append(cd)

        #Retornamos la lista de objetos de cds
        return cds

    def insertData(self, dataSource, cd):
        #Generamos la Query
        query = ("INSERT INTO cd VALUES(NULL,'%s','%s',%i,%i,%i)" %(cd.getTitulocd(),\
            cd.getFechacompra(),cd.getIdcomprador(),cd.getIddiscagraficas(),cd.getIdidioma()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def upDate(self, dataSource, cd):
        #Generamos la Query
        query = ("UPDATE cd SET Titulo='%s', Fecha='%s', idcomprador= %i,\
            iddiscograficas= %i, ididioma= %i,WHERE idCD=%i" %(cd.getTitulocd(),\
            cd.getFechacompra(),cd.getIdcomprador(),cd.getIddiscagraficas(),cd.getIdidioma(),cd.getIdCD()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def deleteDate(self, dataSource, cd):

        query = ("DELETE FROM cd WHERE idCD=%i" %(cd.getIdCD()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)