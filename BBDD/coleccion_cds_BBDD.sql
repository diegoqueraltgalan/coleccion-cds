CREATE DATABASE  IF NOT EXISTS `coleccion_cds` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `coleccion_cds`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: coleccion_cds
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autores` (
  `idautores` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Artistico` varchar(45) DEFAULT NULL,
  `Nacionalidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idautores`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'Linkin Park','EEUU'),(2,'Ne-Yo','EEUU'),(3,'Aerosmith','EEUU'),(4,'Alexander Abreu','Cuba'),(5,'Romeo Santos','Republica Dominicana'),(6,'David Guetta','Francia'),(7,'Ed Sheeran','United Kindong');
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd`
--

DROP TABLE IF EXISTS `cd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd` (
  `idCD` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(45) NOT NULL,
  `Fecha` date NOT NULL,
  `idcomprador` int(11) DEFAULT NULL,
  `iddiscograficas` int(11) DEFAULT NULL,
  `ididioma` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCD`),
  KEY `FK_CD_COMPRADOR_idx` (`idcomprador`),
  KEY `FK_CD_AUTORES_idx` (`ididioma`),
  CONSTRAINT `FK_CD_AUTORES` FOREIGN KEY (`ididioma`) REFERENCES `autores` (`idautores`),
  CONSTRAINT `FK_CD_COMPRADOR` FOREIGN KEY (`idcomprador`) REFERENCES `comprador` (`idcomprador`),
  CONSTRAINT `FK_CD_IDIOMAS` FOREIGN KEY (`ididioma`) REFERENCES `idiomas` (`ididiomas`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd`
--

LOCK TABLES `cd` WRITE;
/*!40000 ALTER TABLE `cd` DISABLE KEYS */;
INSERT INTO `cd` VALUES (1,'METEORA','2003-03-28',5,1,1),(2,'Hybrid Theory','2000-05-15',5,1,1),(3,'Because of  You','2007-06-24',2,2,1),(4,'Year of the Gentleman','2008-08-18',3,2,1),(5,'AEROSMITH','1980-12-22',1,1,1),(6,'Pasaporte','2012-05-28',3,4,2),(7,'Utopia','2019-02-14',5,3,2),(8,'Nothing But The Beat','2011-08-15',4,3,1),(9,'Divide','2017-12-24',5,4,1);
/*!40000 ALTER TABLE `cd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_autores`
--

DROP TABLE IF EXISTS `cd_autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_autores` (
  `idcd` int(11) NOT NULL,
  `idautores` int(11) NOT NULL,
  PRIMARY KEY (`idcd`),
  KEY `FK_CD_AUTORES_AUTORES_idx` (`idautores`),
  CONSTRAINT `FK_CD_AUTORES_AUTORES` FOREIGN KEY (`idautores`) REFERENCES `autores` (`idautores`),
  CONSTRAINT `FK_CD_AUTORES_CD` FOREIGN KEY (`idcd`) REFERENCES `cd` (`idCD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_autores`
--

LOCK TABLES `cd_autores` WRITE;
/*!40000 ALTER TABLE `cd_autores` DISABLE KEYS */;
INSERT INTO `cd_autores` VALUES (1,1),(2,1),(3,2),(4,2),(5,3),(6,4),(7,5),(8,6),(9,7);
/*!40000 ALTER TABLE `cd_autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_generos`
--

DROP TABLE IF EXISTS `cd_generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_generos` (
  `idcd` int(11) NOT NULL,
  `idgeneros` int(11) NOT NULL,
  PRIMARY KEY (`idcd`,`idgeneros`),
  KEY `FK_CD_GENEROS_GENEROS_idx` (`idgeneros`),
  CONSTRAINT `FK_CD_GENEROS_CD` FOREIGN KEY (`idcd`) REFERENCES `cd` (`idCD`),
  CONSTRAINT `FK_CD_GENEROS_GENEROS` FOREIGN KEY (`idgeneros`) REFERENCES `generos` (`idgeneros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_generos`
--

LOCK TABLES `cd_generos` WRITE;
/*!40000 ALTER TABLE `cd_generos` DISABLE KEYS */;
INSERT INTO `cd_generos` VALUES (1,1),(2,1),(5,1),(3,2),(4,2),(9,3),(3,4),(4,4),(8,5),(7,6),(6,7);
/*!40000 ALTER TABLE `cd_generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprador`
--

DROP TABLE IF EXISTS `comprador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comprador` (
  `idcomprador` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcomprador`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprador`
--

LOCK TABLES `comprador` WRITE;
/*!40000 ALTER TABLE `comprador` DISABLE KEYS */;
INSERT INTO `comprador` VALUES (1,'Amilcar','Vaca'),(2,'Grace','Vaca'),(3,'Pablo','Guevara'),(4,'Javier','Constante'),(5,'Diego','Vaca');
/*!40000 ALTER TABLE `comprador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discograficas`
--

DROP TABLE IF EXISTS `discograficas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `discograficas` (
  `iddiscograficas` int(11) NOT NULL AUTO_INCREMENT,
  `Discografica` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddiscograficas`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discograficas`
--

LOCK TABLES `discograficas` WRITE;
/*!40000 ALTER TABLE `discograficas` DISABLE KEYS */;
INSERT INTO `discograficas` VALUES (1,'Columbia Recods'),(2,'Universal Music Group'),(3,'Sony Music'),(4,'Warner Music Group');
/*!40000 ALTER TABLE `discograficas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos`
--

DROP TABLE IF EXISTS `generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `generos` (
  `idgeneros` int(11) NOT NULL AUTO_INCREMENT,
  `Genero` varchar(45) NOT NULL,
  PRIMARY KEY (`idgeneros`),
  UNIQUE KEY `Genero_UNIQUE` (`Genero`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos`
--

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` VALUES (6,'Bachata'),(5,'Electronica'),(4,'Hip-Hop'),(3,'Pop'),(2,'R&B'),(1,'Rock'),(7,'Salsa');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `idiomas` (
  `ididiomas` int(11) NOT NULL AUTO_INCREMENT,
  `Idioma` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ididiomas`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idiomas`
--

LOCK TABLES `idiomas` WRITE;
/*!40000 ALTER TABLE `idiomas` DISABLE KEYS */;
INSERT INTO `idiomas` VALUES (1,'Ingles'),(2,'Español'),(3,'Frances'),(4,'Italiano');
/*!40000 ALTER TABLE `idiomas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-17 10:52:08
