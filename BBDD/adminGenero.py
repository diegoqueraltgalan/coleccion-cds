#Administración BBDD
import mysql.connector
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import * 
from BBDD.adminDiscograficas import * 
from BBDD.adminCds import * 

class AdminGenero:

    def getAll(self,dataSource):
        #Generamos la Query
        query = ("SELECT * FROM generos")
        #Recogemos los datos que nos devuelve la consulta
        generos_bd = dataSource.executeQuerySelect(query)
        #Creamos lista vacia para guardar objetos de los generos
        generos = []
        #Recorremos los datos recogidos anteriormente (matriz)
        for gen in generos_bd:
            #Creamos objetos de los generos 
            genero = Genero(gen[0],gen[1])
            #Los guardamos en la lista generos
            generos.append(genero)

        #Retornamos la lista de objetos de generos
        return generos

    def getById(self,dataSource, id):
        #Generamos la Query
        query = ("SELECT * FROM generos WHERE idgeneros = %i" %(id))

        #Recogemos los datos que nos devuelve la consulta
        generos_bd = dataSource.executeQuerySelect(query)

        #Generamos un genero como none
        genero = None
        #Comprobamos que la consulta solo nos devulve una columna
        if len(generos_bd) == 1:
            #Recorremos los datos recogidos anteriormente (matriz)
            for gen in generos_bd:
                #Generamos el genero a partir del registro 
                genero = Genero(gen[0],gen[1])

        #Retornamos el genero encontrado
        return genero

    def getGenerosByCD(self,dataSource, idCD):
        #Generamos la Query
        query = ("SELECT generos.* FROM generos JOIN cd_generos ON generos.idgeneros = cd_generos.idgeneros WHERE idcd = %s" %(idCD))
        #Recogemos los datos que nos devuelve la consulta
        generos_bd = dataSource.executeQuerySelect(query)
        #Creamos lista vacia para guardar objetos de los generos
        generos = []
        #Recorremos los datos recogidos anteriormente (matriz)
        for gen in generos_bd:
            #Creamos objetos de los generos 
            genero = Genero(gen[0],gen[1])
            #Los guardamos en la lista generos
            generos.append(genero)

        #Retornamos la lista de objetos de generos
        return generos


    def insertData(self, dataSource, genero):
        #Generamos la Query
        query = ("INSERT INTO generos VALUES(NULL,'%s')" %(genero.getGenero()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def upDate(self, dataSource, genero):
        #Generamos la Query
        query = ("UPDATE generos SET genero='%s' WHERE idgeneros=%i" %(genero.getGenero(),genero.getIdgeneros()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def deleteDate(self, dataSource, genero):

        query = ("DELETE FROM generos WHERE idgeneros=%i" %(genero.getIdgeneros()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)