#Administración BBDD
import mysql.connector
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import *
from BBDD.adminCds import * 

class AdminDiscografica:

    def getAll(self, dataSource):
        #Generamos la Query
        query = ("SELECT * FROM discograficas")
        #Recogemos los datos que nos devuelve la consulta
        adminCds = AdminCds()
        discograficas_bd = dataSource.executeQuerySelect(query)
        #Creamos lista vacia para guardar objetos de los discograficas
        discograficas = []
        #Recorremos los datos recogidos anteriormente (matriz)
        for dis in discograficas_bd:
            #Creamos objetos de los discograficas 
            discografica = Discografica(dis[0],dis[1])
            discografica.setCds(adminCds.getCDsByDiscografica(dataSource, discografica.getIddiscograficas()))
            #Los guardamos en la lista discograficas
            discograficas.append(discografica)
        #cerramos cursor   
        cursor.close()
        #Retornamos la lista de objetos de discograficas
        return discograficas

    def getById(self, dataSource, id):
        #Generamos la Query
        query = ("SELECT * FROM discograficas WHERE iddiscograficas = %i" %(id))
        #Recogemos los datos que nos devuelve la consulta
        discograficas_bd = dataSource.executeQuerySelect(query)
        #Generamos un discografica como none
        discografica = None
        #Comprobamos que la consulta solo nos devulve una columna
        if len(discograficas_bd) == 1:
            #Recorremos los datos recogidos anteriormente (matriz)
            for dis in discograficas_bd:
                #Generamos el discografica a partir del registro 
                discografica = Discografica(dis[0],dis[1])
            #cerramos cursor   
        cursor.close()
        #Retornamos el discografica encontrado
        return discografica

    def insertData(self,dataSource, discografica):
        #Generamos la Query
        query = ("INSERT INTO discograficas VALUES(NULL,'%s')" %(discografica.getDiscografica()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def upDate(self,discografica):
        #Generamos la Query
        query = ("UPDATE discograficas SET discografica='%s' WHERE iddiscograficas=%i" %(discografica.getDiscografica(),discografica.getIddiscograficas()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)

    def deleteDate(self,discografica):
        #Generamos la Query
        query = ("DELETE FROM discograficas WHERE iddiscograficas=%i" %(discografica.getIddiscograficas()))

        #Ejecutamos la Query y comprobamos que todo haya ido bien
        return dataSource.executeOtherQuery(query)


        
