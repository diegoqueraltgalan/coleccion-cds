import mysql.connector

class DataSource:
	def __init__(self):
		self.__cnx = mysql.connector.connect(user='root', password='root',host='localhost', database='coleccion_cds')
		#self.__cnx = mysql.connector.connect(user='root', password='root',host='localhost', database='coleccion_cds')
		self.__cnx.autocommit = False

	def executeQuerySelect(self, query):
		#Generamos el crsor
		cursor = self.__cnx.cursor()
		#Ejecutamos Querry.
		cursor.execute(query)
		#Recogemos los datos que nos devuelve la consulta
		result = cursor.fetchall()

		#cerramos cursor
		cursor.close()
		#Devolvemos el resultado
		return result

	def executeOtherQuery(self, query):
		#Generamos el crsor
		cursor = self.__cnx.cursor()
		#Ejecutamos Querry.
		cursor.execute(query)
		fallo = False
		if cursor.rowcount<1:
			fallo = True

		cursor.close()
		return fallo

	def commitAction(self):
		self.__cnx.commit()

	def rollbackAction(self):
		self.__cnx.rollback()

	def closeConnection(self):
		self.__cnx.close() 