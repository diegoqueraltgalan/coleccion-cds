#Main Menú Principal
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import * 
from BBDD.adminDiscograficas import * 
from BBDD.adminCds import * 
from Menus.Menu_Discograficas import *
from Menus.Menu_Generos import *
from Menus.Menu_CD import *

opcion=-1
while opcion!=0:
		
    print("***********COLECCION CD'S AI***************\n")
    opcion=int(input("Elige una opción:\n1.Genero\n2.Discográficas\n3.CD\n0. Salir\n"))
    print("****************************************")
    if opcion==1:
    	menuGeneros()
                                                
    elif opcion==2:
    	menuDiscografica()

    elif opcion==3:
        menuCds()

    elif opcion==0:
        print("Saliendo...")
    else:
        print("Opción no válida.")
