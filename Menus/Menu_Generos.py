#Menu Generos
from clases.genero_class import *
from BBDD.adminGenero import *
def menuGeneros():
	opciong=-1
	while  opciong!=0:
	    print("************MENU GENEROS*******************")
	    opciong=int(input("Elige una opción:\n1: Insertar Nuevo Genero\n2: Actualizar Un Genero\n3: Listar Generos\n4: Borrar Genero\n5: Mostrar Un Genero por ID\n 0. Salir\n"))
	    if opciong==1:
	    	print("************INSERTAR GENEROS*******************")
	    	nwgenero=input("Introducir Nuevo Genero: ")
	    	#Creamos Objeto Genero            	
	    	genero=Genero(None,nwgenero)
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admgenero=AdminGenero()
	    	#Comprobamos si ha ido correctamente la operación
	    	if not admgenero.insertData(dataSource, genero):
	    		print("Se ha insertado correctamente.")
	    		dataSource.commitAction()
	    	else:
	    		print("Ha habido un fallo al insertas")
	    		dataSource.rollbackAction()
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()
	    elif opciong==2:
	    	print("************ACTUALIZAR GENEROS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD            	
	    	dataSource = DataSource()
	    	admgenero=AdminGenero()
	    	generos_act=admgenero.getAll(dataSource)
	    	idsgeneros = []
	    	for g in generos_act:
	    		idsgeneros.append(int(g.getIdgeneros()))
	    		print(g)

	    	oldgeneroid=int(input("Genero Que Desea Modificar (Introduce el numero): "))

	    	if oldgeneroid in idsgeneros:
	    		newgenero = input("Introduce el nuevo nombe del genero: ")
	    		#Creamos Objeto Genero
	    		genero = Genero(oldgeneroid,newgenero)
	    		#Comprobamos si ha ido correctamente la operación
	    		if not admgenero.upDate(dataSource, genero):
	    			print("Se ha actualizado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al actualizarlo.")
	    			dataSource.rollbackAction()
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciong==3:
	    	print("************LISTAR GENEROS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admgenero=AdminGenero()
	    	generos_act=admgenero.getAll(dataSource)
	    	for g in generos_act:
	    		print(g)
	    	dataSource.closeConnection()
	    elif opciong==4:
	    	print("************BORRAR GENEROS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admgenero=AdminGenero()
	    	generos_act=admgenero.getAll(dataSource)
	    	idsgeneros = []
	    	for g in generos_act:
	    		idsgeneros.append(int(g.getIdgeneros()))
	    		print(g)

	    	oldgeneroid=int(input("Genero Que Desea Borrar (Introduce el numero): "))

	    	if oldgeneroid in idsgeneros:
	    		genero = Genero(oldgeneroid,None)
	    		if not admgenero.deleteDate(dataSource, genero):
	    			print("Se ha borrado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al borrar.")
	    			dataSource.rollbackAction()
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciong==5:
	    	print("************VER GENEROS POR ID*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admgenero=AdminGenero()
	    	idgenero=int(input("Introduce el id del genero que desea ver: "))
	    	genero=admgenero.getById(dataSource, idgenero)

	    	if genero != None:
	    		print(genero)
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()
	    elif opciong==0:
	        print("Regresando Al Menu Principal")
	    else:
	        print("Opcion no válida")