#Menu cds
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import * 
from BBDD.adminDiscograficas import * 
from BBDD.adminCds import * 

def menuCds():
	opciong=-1
	while  opciong!=0:
	    print("************MENU CD'S*******************")
	    opciong=int(input("Elige una opción:\n1: Insertar Nuevo CD\n2: Actualizar Un CD\n3: Listar CD's\n4: Borrar CD\n5: Mostrar Un CD por ID\n 0. Salir\n"))
	    if opciong==1:
	    	print("************INSERTAR CD*******************")
	    	titulocd=input("Introducir Tiulo del CD: ")
	    	fecha_compra=input("Introducir Fecha de Compra de CD (YYYY-MM-DD): ")
	    	id_comprador=5
	    	dataSource = DataSource()
	    	admdsc=AdminDiscografica()
	    	cds_act=admdsc.getAll(dataSource)
	    	idsdiscograficas = []
	    	for g in discograficas_act:
	    		idsdiscograficas.append(int(g.getIddiscograficas()))
	    		print(g)
	    	valido = False
	    	while valido:
	    		id_discograficas=int(input("Introducir ID de la Discongráfica: "))
	    		if id_discograficas in idsdiscograficas:
	    			valido = True
	    		else:
	    			print("ID No Valido Vuelva a Ingresarlo")
	    	id_idioma= 1

	    	#Creamos Objeto cd            	
	    	cd=Cd(None,nwcd,fecha_compra,id_comprador,id_discograficas,id_idioma)
	    	admcd=AdminCds()
	    	#Comprobamos si ha ido correctamente la operación
	    	if not admcd.insertData(dataSource, cd):
	    		print("Se ha insertado correctamente.")
	    		dataSource.commitAction()
	    	else:
	    		print("Ha habido un fallo al insertas")
	    		dataSource.rollbackAction()
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()
	    elif opciong==2:
	    	print("************ACTUALIZAR CD*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD            	
	    	dataSource = DataSource()
	    	admcd=AdminCds()
	    	cds_act=admcd.getAll(dataSource)
	    	idscds = []
	    	for g in cds_act:
	    		idscds.append(int(g.getIdcds()))
	    		print(g)

	    	oldcdid=int(input("cd Que Desea Modificar (Introduce el numero): "))

	    	if oldcdid in idscds:
	    		newcd = input("Introduce El Nuevo Titulo del cd: ")
	    		new_fecha_compra=input("Introducir Nueva Fecha de Compra de CD (YYYY-MM-DD): ")
	    		new_id_comprador=int(input("Introducir Nuevo Comprador (ID): "))
	    		new_id_discograficas=int(input("Introducir Nuevo ID de la Discongráfica: "))
	    		new_id_idioma=int(input("Introducir Nuevo ID del Idioma: "))
	    		#Creamos Objeto cd
	    		cd = Cd(oldcdid,newcd,new_fecha_compra,new_id_comprador,new_id_discograficas,new_id_idioma)
	    		#Comprobamos si ha ido correctamente la operación
	    		if not admcd.upDate(dataSource, cd):
	    			print("Se ha actualizado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al actualizarlo.")
	    			dataSource.rollbackAction()
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciong==3:
	    	print("************LISTAR CD'S*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admcd=AdminCds()
	    	cds_act=admcd.getAll(dataSource)
	    	for g in cds_act:
	    		print(g)
	    	dataSource.closeConnection()
	    elif opciong==4:
	    	print("************BORRAR cdS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admcd=AdminCds()
	    	cds_act=admcd.getAll(dataSource)
	    	idscds = []
	    	for g in cds_act:
	    		idscds.append(int(g.getIdcds()))
	    		print(g)

	    	oldcdid=int(input("CD Que Desea Borrar (Introduce el numero): "))

	    	if oldcdid in idscds:
	    		cd = Cd(oldcdid,None,None,None,None,None)
	    		if not admcd.deleteDate(dataSource, cd):
	    			print("Se ha borrado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al borrar.")
	    			dataSource.rollbackAction()
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciong==5:
	    	print("************VER CD'S POR ID*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admcd=AdminCds()
	    	idcd=int(input("Introduce el id del cd que desea ver: "))
	    	cd=admcd.getById(dataSource, idcd)

	    	if cd != None:
	    		print(cd)
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()
	    elif opciong==0:
	        print("Regresando Al Menu Principal")
	    else:
	        print("Opcion no válida")