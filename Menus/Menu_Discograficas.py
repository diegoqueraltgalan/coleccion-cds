#Menu Discográficas
from clases.genero_class import *
from clases.discografica_class import * 
from clases.cd_class import * 
from BBDD.DataSource import * 
from BBDD.adminGenero import * 
from BBDD.adminDiscograficas import * 
from BBDD.adminCds import * 

def menuDiscografica():
	opciond=-1
	while opciond!=0:
	    print("************MENU DISCOGRÁFICAS*******************")
	    opciond=int(input("Elige una opción:\n1: Insertar Nuevo Discográfica\n2: Actualizar Un Discográfica\n3: Listar Discográficas\n4: Borrar Discografica\n5: Mostrar Un Discografica por ID\n 0. Salir\n"))
	    if opciond==1:
	    	print("************INSERTAR DISCOGRÁFICAS*******************")
	    	nwdiscografica=input("Introducir Nuevo Discográfica: ")
	    	#Creamos Objeto discografica            	
	    	discografica=Discografica(None,nwdiscografica)
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admdiscografica=AdminDiscografica()
	    	#Comprobamos si ha ido correctamente la operación
	    	if not admdiscografica.insertData(dataSource, discografica):
	    		print("Se ha insertado correctamente.")
	    		dataSource.commitAction()
	    	else:
	    		print("Ha habido un fallo al insertas")
	    		dataSource.rollbackAction()
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()
	    elif opciond==2:
	    	print("************ACTUALIZAR DISCOGRÁFICAS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD            	
	    	dataSource = DataSource()
	    	admdiscografica=AdminDiscografica()
	    	discograficas_act=admdiscografica.getAll(dataSource)
	    	idsdiscograficas = []
	    	for g in discograficas_act:
	    		idsdiscograficas.append(int(g.getIddiscograficas()))
	    		print(g)

	    	olddiscograficaid=int(input("Discografica Que Desea Modificar (Introduce el número): "))

	    	if olddiscograficaid in idsdiscograficas:
	    		newdiscografica = input("Introduce el nuevo nombe del Discografica: ")
	    		#Creamos Objeto discografica
	    		discografica = Discografica(olddiscograficaid,newdiscografica)
	    		#Comprobamos si ha ido correctamente la operación
	    		if not admdiscografica.upDate(dataSource, discografica):
	    			print("Se ha actualizado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al actualizarlo.")
	    			dataSource.rollbackAction()
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciond==3:
	    	print("************LISTAR DISCOGRÁFICAS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admdiscografica=AdminDiscografica()
	    	discograficas_act=admdiscografica.getAll(dataSource)
	    	for g in discograficas_act:
	    		print(g)
	    	dataSource.closeConnection()
	    elif opciond==4:
	    	print("************BORRAR DISCOGRÁFICAS*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admdiscografica=AdminDiscografica()
	    	discograficas_act=admdiscografica.getAll(dataSource)
	    	idsdiscograficas = []
	    	for g in discograficas_act:
	    		idsdiscograficas.append(int(g.getIddiscograficas()))
	    		print(g)

	    	olddiscograficaid=int(input("discografica Que Desea Borrar (Introduce el numero): "))

	    	if olddiscograficaid in idsdiscograficas:
	    		discografica = Discografica(olddiscograficaid,None)
	    		if not admdiscografica.deleteDate(dataSource, discografica):
	    			print("Se ha borrado correctamente.")
	    			dataSource.commitAction()
	    		else:
	    			print("Ha habido un fallo al borrar.")
	    			dataSource.rollbackAction()

	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	dataSource.closeConnection()

	    elif opciond==5:
	    	print("************VER DISCOGRÁFICAS POR ID*******************")
	    	#Generamos el Objeto Para Crear la conexión a la BBDD
	    	dataSource = DataSource()
	    	admdiscografica=AdminDiscografica()
	    	iddiscografica=int(input("Introduce el id del discografica que desea ver: "))
	    	discografica=admdiscografica.getById(dataSource, iddiscografica)

	    	if discografica != None:
	    		print(discografica)
	    	else:
	    		print("Ese id no existe polluelo!")
	    		#Cerramos conexión de la BBDD
	    	admdiscografica.closeConnection()
	    elif opciond==0:
	        print("Regresando Al Menu Principal")
	    else:
	        print("Opcion no válida")