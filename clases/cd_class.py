#Class titulocd.
from clases.genero_class import * 
from clases.discografica_class import * 

class Cd:
    def __init__(self,idCD,titulocd,fechacompra,idcomprador,iddiscagraficas,ididioma):
        self.__idCD = idCD
        self.__titulocd = titulocd
        self.__fechacompra = fechacompra
        self.__idcomprador = idcomprador
        self.__iddiscagraficas = iddiscagraficas
        self.__ididioma = ididioma
        self.__discografica = None 
        self.__generos = []

    def setIdCD(self,idCD):
        self.__idCD=idCD

    def setTitulocd(self,titulocd):
        self.__titulocd = titulocd

    def setFechacompra(self,fechacompra):
        self.__fechacompra = fechacompra

    def setIdcomprador(self,idcomprador):
        self.__idcomprador = idcomprador

    def setIddiscagraficas(self,iddiscagraficas):
        self.__iddiscagraficas = iddiscagraficas

    def setIdidioma(self,ididioma):
        self.__ididioma = ididioma

    def setDiscografica(self,discografica):
        self.__discografica = discografica
        self.__iddiscagraficas = discografica.getIddiscograficas()

    def setGeneros(self,generos):
        self.__generos = generos

    def getIdCD(self):
        return self.__idCD

    def getTitulocd(self):
        return self.__titulocd

    def getFechacompra(self):
        return self.__fechacompra

    def getIdcomprador(self):
        return self.__idcomprador

    def getIddiscagraficas(self):
        return self.__iddiscagraficas

    def getIdidioma(self):
        return self.__ididioma

    def getDiscografica(self):
        return self.__discografica

    def getGeneros(self):
        return self.__generos

    def __str__(self):
        generos = " ".join(str(e) for e in self.__generos)
        text =  "id titulocds: "+str(self.__idCD)+"\ttitulocd: "+str(self.__titulocd)+\
                "\t fechacompra: "+str(self.__fechacompra)+"\t idcomprador: "+str(self.__idcomprador)+\
                "\t iddiscagraficas: "+str(self.__iddiscagraficas)+" ididioma: "+str(self.__ididioma)+\
                "\nDiscografica: "+str(self.__discografica)+"\nGeneros: \n"+str(generos)
        return text
    
